
const mongoose = require("mongoose");

module.exports = () => {
	const connectionParams = {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	};
	try {
		mongoose.connect(process.env.DBURL,connectionParams);
		console.log("Hi Developer Connected to database successfully");
		
	} catch (error) {
		console.log(error);
		
		console.log("Could not connect database!");
	}
};


