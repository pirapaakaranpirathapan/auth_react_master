
const Users = require("../../models/auth/User");
const jwt = require('jsonwebtoken')

const userController = {

    //register
    register: async (req, res) => {

        
        try {
            const { name, email, password } = req.body
            //console.log(req.body)
            if (!name || !email || !password) {
                return res.status(400).json({ msg: "Please Fill fields" })
            }
            if (!validateEmail(email)) {
                return res.status(400).json({ msg: "Invalid Email" })
            }

            const user = await Users.findOne({ email })
            if (user) {
                return res.status(400).json({ msg: "Already exists" })
            }

            if (password.length < 6) {
                return res.status(400).json({ msg: "Password 6 must" })

            }


            const newUser = new Users({
                name, email, password
            })
            //console.log(newUser);
            newUser.save()


            //const activation_token = createActivationToken(newUser);
            //const url = `${CLIENT_URL}/user/activate/${activation_token}`

            //sendMail(email,url)
            //console.log("activation_token:",activation_token);
            res.json({ msg: "Register Success" })


        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },

    //login
    login: async (req, res) => {
        try {
            const { email, password } = req.body
            const user = await Users.findOne({ email })
            if (!user) {
                return res.status(400).json({ msg: "this email does not exist" })
            }
            const isMatch = await Users.findOne({ password })

            if (!isMatch) {
                return res.status(400).json({ msg: "password is incorrect" })
            }
            
            const token = jwt.sign(
                {
                    name: user.name,
                    email: user.email,
                },
                'hashfood'
            )

            res.json({ msg: "login success!", user, token:token })

        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },

    

}















function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



module.exports = userController