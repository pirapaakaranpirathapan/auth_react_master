require('dotenv').config();
const express = require("express");
const cors = require("cors")

const database = require("./database/mongoDbDatabase")
const userAuthRoute = require("./routes/auth/userRoutes")

const app = express();
app.use(express.json());
app.use(cors());

//routes
app.use('/api',userAuthRoute);

//db connection
database()

//connect port
const PORT = process.env.PORT
app.listen(PORT,()=>{
    console.log("server is Running on port",PORT );
})